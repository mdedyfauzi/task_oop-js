class Movie {
  static rate = 8;

  constructor(title, genre) {
    this.title = title;
    this.genre = genre;
  }

  details() {
    console.log(`
      Title : ${this.title}
      Genre : ${this.genre}
      `);
  }

  static tellRate() {
    return this.rate === this.rate >= 8 ? 'Good' : 'Bad';
  }
}

class Watch extends Movie {
  static reviewer = 'andre';

  constructor(title, genre, orTvTheater, date) {
    super(title, genre);
    this.orTvTheater = orTvTheater;
    this.date = date;
  }

  tv() {
    console.log(`Now playing on ${this.orTvTheater} at ${this.date} :`);
    super.details();
  }

  static comment() {
    console.log(`The movie rate by ${this.reviewer} :`);
    super.tellRate();
  }
}

let watchNow = new Watch('Avangers', 'Action', 'Global TV', 'Oktober 24, 2021');
watchNow.tv();
watchNow.comment();

//NB : help static method can't be called !!!
//eror log : watchNow.comment is not a function
